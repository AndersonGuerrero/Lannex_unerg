from django.conf.urls import url

from views import (Index, QuienesSomos, Portafolio, Contacto, Ayuda,
                   ZonasCobertura, FechaCorte, DatosBancarios)

urlpatterns = [
    url(r'^$', Index.as_view()),
    url(r'^quienes-somos', QuienesSomos.as_view(), name='web_quienes_somos'),
    url(r'^portafolio', Portafolio.as_view(), name='portafolio'),
    url(r'^contacto', Contacto.as_view(), name='contacto'),
    url(r'^ayuda', Ayuda.as_view(), name='ayuda'),
    url(r'^zonas-de-cobertura', ZonasCobertura.as_view(),
        name='zonas_cobertura'),
    url(r'^fecha-de-corte', FechaCorte.as_view(), name='fecha_corte'),
    url(r'^datos-bancarios', DatosBancarios.as_view(), name='datos_bancarios'),
]
