from django.contrib import admin
from models import Noticia, Producto

admin.site.register(Noticia)
admin.site.register(Producto)
