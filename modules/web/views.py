# -*- coding: utf-8 -*-
from django.views.generic import TemplateView


class Index(TemplateView):
    template_name = "web_index.html"


class QuienesSomos(TemplateView):
    template_name = "quienes_somos.html"


class Portafolio(TemplateView):
    template_name = "portafolio.html"


class Contacto(TemplateView):
    template_name = "contacto.html"


class Ayuda(TemplateView):
    template_name = "ayuda.html"


class ZonasCobertura(TemplateView):
    template_name = "zonas_cobertura.html"


class FechaCorte(TemplateView):
    template_name = "fecha_corte.html"


class DatosBancarios(TemplateView):
    template_name = "datos_bancarios.html"
