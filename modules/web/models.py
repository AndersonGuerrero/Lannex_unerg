# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from base.models import Persona, Imagen

from django.db import models


class Noticia(models.Model):
    persona = models.ForeignKey(Persona)
    titulo = models.CharField(max_length=100)
    contenido = models.TextField()
    imagenes = models.ManyToManyField(Imagen)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Noticia"
        verbose_name_plural = "Noticias"
        db_table = 'web_noticia'

    def __unicode__(self):
        return "%s - %s" % (self.persona, self.titulo)


class Producto(models.Model):
    persona = models.ForeignKey(Persona)
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    precio = models.FloatField()
    imagenes = models.ManyToManyField(Imagen)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "producto"
        verbose_name_plural = "productos"
        db_table = 'web_producto'

    def __unicode__(self):
        return "%s - %s" % (self.persona, self.nombre)
