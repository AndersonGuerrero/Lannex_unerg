# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from base.models import Persona


class Plan(models.Model):
    TIPO_CHOICES = (("Doméstico", "Doméstico"), ("Empresarial", "Empresarial"))
    tipo = models.CharField(max_length=6, choices=TIPO_CHOICES, blank=True, null=True)
    precio = models.FloatField()
    descripcion = models.TextField()
    vel_sub_kb = models.IntegerField()
    vel_baj_kb = models.IntegerField()

    class Meta:
        verbose_name = "Plan"
        verbose_name_plural = "Planes"
        db_table = 'online_plan'

    def __unicode__(self):
        return "%s - %s  velocidad: %s Kbs - %s Kbs" % (self.tipo, self.precio,
                                                        self.vel_baj_kb,
                                                        self.vel_sub_kb)


class Contrato(models.Model):
    persona = models.ForeignKey(Persona)
    plan = models.ForeignKey(Plan)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Contrato"
        verbose_name_plural = "Contratos"
        db_table = 'online_contrato'

    def __unicode__(self):
        return "%s - %s" % (self.persona, self.plan)


class Reporte(models.Model):
    ESTADO_CHOICES = (("Espera", "Espera"), ("Proceso", "Proceso"),
                      ("Solventado", "Solventado"))
    persona = models.ForeignKey(Persona)
    contenido = models.TextField()
    estado = models.CharField(max_length=6, choices=ESTADO_CHOICES,
                              blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Reporte"
        verbose_name_plural = "Reportes"
        db_table = 'online_reporte'

    def __unicode__(self):
        return "%s - %s" % (self.persona, self.estado)


class Notificacion(models.Model):
    CORRESPONDE_CHOICES = (("Mensualidad", "Mensualidad"),
                           ("Instalacion", "Instalacion"),
                           ("Soporte tecnico", "Soporte tecnico"))
    TIPO_PAGO_CHOICES = (("Deposito", "Deposito"),
                         ("Transferencia", "Transferencia"),
                         ("Efectivo", "Efectivo"))
    persona = models.ForeignKey(Persona)
    monto_pagado = models.FloatField()
    corresponde = models.CharField(max_length=15, choices=CORRESPONDE_CHOICES)
    tipo_pago = models.CharField(max_length=15, choices=TIPO_PAGO_CHOICES)
    descripcion_soporte = models.TextField(null=True, blank=True)
    fecha_pago = models.DateField()
    mensaje_libre = models.TextField()
    correcta = models.BooleanField(default=False)
    fecha_revision = models.DateField(blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Notificaciones"
        db_table = 'online_notificacion'

    def __unicode__(self):
        return "%s - %s" % (self.persona, self.corresponde)


class Mensaje(models.Model):
    TIPO_CHOICES = (("Critica", "Critica"), ("Recomendacion", "Recomendacion"))
    persona = models.ForeignKey(Persona)
    contenido = models.TextField()
    leido = models.BooleanField(default=False)
    tipo = models.CharField(max_length=15, choices=TIPO_CHOICES,
                            blank=True, null=True)
    respuesta = models.TextField()

    class Meta:
        verbose_name = "Mensajes"
        verbose_name_plural = "Mensajes"
        db_table = 'online_mensaje'

    def __unicode__(self):
        return "%s - %s" % (self.persona, self.tipo)
