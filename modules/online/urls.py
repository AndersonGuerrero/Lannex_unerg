from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from views import (NuevaNotificacion, Index)

urlpatterns = [
    url(r'^$', login_required(Index.as_view()), name='index-online'),
    url(r'^notificaciones/nueva', login_required(NuevaNotificacion.as_view()),
        name='nueva-notificacion')
]
