from django.contrib import admin

from models import Plan, Reporte, Notificacion, Contrato, Mensaje

admin.site.register(Plan)
admin.site.register(Reporte)
admin.site.register(Notificacion)
admin.site.register(Mensaje)
admin.site.register(Contrato)
