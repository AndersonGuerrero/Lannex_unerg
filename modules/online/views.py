from django.views.generic import TemplateView
from django.template import RequestContext
from django.shortcuts import render_to_response


class NuevaNotificacion(TemplateView):
    template_name = "nueva_notificacion.html"

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))


class Index(TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))
