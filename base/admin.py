from django.contrib import admin
from models import Persona, Imagen, Zona

admin.site.register(Persona)
admin.site.register(Imagen)
admin.site.register(Zona)
