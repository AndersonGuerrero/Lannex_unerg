# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Zona(models.Model):
    nombre = models.CharField(max_length=500)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Zona"
        verbose_name_plural = "Zonas"
        db_table = 'b_zona'

    def __unicode__(self):
        return "%s - %s" % (self.nombre, self.estado)


class Imagen(models.Model):
    nombre = models.CharField(max_length=500)
    url = models.CharField(max_length=500)

    class Meta:
        verbose_name = "Imagen"
        verbose_name_plural = "Imagenes"
        db_table = 'b_imagen'

    def __unicode__(self):
        return "%s - %s" % (self.nombre, self.url)


class Persona(models.Model):
    usuario = models.OneToOneField(User, unique=True)
    cedula = models.IntegerField()
    direccion = models.TextField()
    zona = models.ForeignKey(Zona, blank=True, null=True)
    telefono = models.CharField(max_length=50)
    celular = models.CharField(max_length=50)
    imagen = models.ForeignKey(Imagen)

    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"
        db_table = 'b_persona'

    def __unicode__(self):
        return "%s - %s" % (self.usuario.first_name, self.usuario.last_name)
