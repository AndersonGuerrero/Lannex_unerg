# -*- coding: utf-8 -*-
# from django.shortcuts import render
import json

from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.models import User


class Login(TemplateView):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(request.GET.get("next", False))
        return render_to_response(self.template_name, {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        logout(request)
        if not request.GET.get("next", False):
            return HttpResponseRedirect('/')
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        try:
            user = authenticate(
                    username=User.objects.get(email__iexact=email).username,
                    password=password)
        except:
            user = None
        if user is not None:
            if user.is_active:
                login(request, user)
                resp = {'msj': "Datos validos :)",
                        'error': False,
                        'url_redirect': request.GET['next'],
                        'icono': 'icon fa fa-check',
                        'type': 'alert-success'}
            else:
                resp = {'msj': "Contraseña valida, \
                                pero su Cuenta esta deshabilitada!.",
                        'error': True,
                        'url_redirect': False,
                        'icono': 'icon fa fa-warning',
                        'type': 'alert-warning'}
        else:
            resp = {'msj': "Email o contraseña incorrectos.",
                    'error': True,
                    'url_redirect': False,
                    'icono': 'icon fa fa-ban',
                    'type': 'alert-danger'}
        return HttpResponse(json.dumps(resp, ensure_ascii=False),
                            content_type='application/json; charset=utf-8')


def lx_logout(request):
    try:
        logout(request)
    except:
        pass
    return HttpResponseRedirect('/')
