$('#form-login').formValidation({
  locale: 'es_CL',
  excluded: [':disabled', ':hidden'],
  row: {
    invalid: 'field-error',
    valid: 'field-good'
  },
  icon: {
    valid: 'fa',
    invalid: 'fa fa-exclamation-triangle',
    validating: 'fa'
  },
  err: {
    container: 'tooltip'
  },
}).on('success.form.fv', function(e) {
  e.preventDefault();
  login();
});

function login(){
  $.ajax({
    url: '',
    type: 'POST',
    dataType: 'json',
    data: $("#form-login").serializeArray(),
  })
  .done(function(json) {
    $("#content-inf").removeClass('lx-hide');
    $("#type-alert").removeAttr('class');
    $("#type-alert").addClass('alert')
    $("#type-alert").addClass('alert-dismissable')
    $("#type-alert").addClass(json.type)
    $("#text").html(json.msj);
    $("#icon").removeAttr('class');
    $("#icon").addClass(json.icono)
    if(json.url_redirect){
      setTimeout(function(){
                $(location).attr('href', json.url_redirect)
              }, 2000);
    }
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
}