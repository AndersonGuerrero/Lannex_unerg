from django.conf.urls import url, include
from django.contrib import admin

from modules.web.views import Index
from base.views import Login, lx_logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', Login.as_view()),
    url(r'^logout/', lx_logout, name='logout'),
    url(r'^$', Index.as_view(), name='index_web'),
    url('^web/', include('modules.web.urls')),
    url('^online/', include('modules.online.urls')),
]
